package com.martin.textinputtest;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    TextInputLayout loginInputLayout;
    EditText loginET;
    TextInputLayout emailInputLayout;
    EditText emailET;
    TextInputLayout siteInputLayout;
    EditText siteET;
    Button clearButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    private void initUI() {
        loginInputLayout = findViewById(R.id.login_text_input);
        loginET = findViewById(R.id.login_edit_text);
        emailInputLayout = findViewById(R.id.email_text_input);
        emailET = findViewById(R.id.email_edit_text);
        siteInputLayout = findViewById(R.id.site_text_input);
        siteET = findViewById(R.id.site_edit_text);
        clearButton = findViewById(R.id.clear_text);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginET.setText("");
                emailET.setText("");
                siteET.setText("");
            }
        });

        final Pattern loginPattern = Pattern.compile("^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\\d.-]{0,19}$");
        loginET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (loginPattern.matcher(editable.toString()).matches()) {
                    loginInputLayout.setError("");
                } else loginInputLayout.setError("In login you can use only letters and numbers");
            }
        });


        final Pattern emailPattern = Pattern.compile("^([a-z0-9_\\.-]+)@([a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$", Pattern.CASE_INSENSITIVE);
        emailET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (emailPattern.matcher(editable.toString()).matches()) {
                    emailInputLayout.setError("");
                } else emailInputLayout.setError("Not email");
            }
        });

        final Pattern sitePattern = Pattern.compile("(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\\.)+[a-zA-Z]{2,63}$)");
        siteET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (sitePattern.matcher(editable.toString()).matches()) {
                    siteInputLayout.setError("");
                } else siteInputLayout.setError("Not domain name");
            }
        });


    }


}
